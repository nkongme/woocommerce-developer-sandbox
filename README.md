# README #

## Lokale Testumgebung einrichten ##
Der Artikel [WordPress Development Workflow](http://www.openstream.ch/developer-blog/wordpress-development-workflow/) erklärt unsere Arbeitsweise mit Git und Bitbucket für WordPress und WooCommerce Projekte. Wenn du diesen Artikel gelesen und verstanden hast, solltest du dir die in diesem Repository befindliche WooCommerce Installation in deiner lokalen Testumgebung einrichten. Du musst nicht zwangsläufig einen Git Client wie Source Tree verwenden. Wenn du sonst auf der Konsole arbeitest, kannst du dies natürlich auch so beibehalten vorausgesetzt du bist mit den grundlegenden Git-Befehlen wie clone, add, commit, pull und push vertraut.  

## Teständerung durchführen und committen ##
### Neue Dateien hinzufügen ###
Wenn die lokale Testumgebung einwandfrei läuft, erstelle bitte ein minimales Child Theme für Twenty Fourteen und erstelle für diese Änderungen einen Pull Request. Zuerst natürlich git add, git commit, git push ausführen bzw. die entsprechenden Schritte mit dem Git Client durchführen. Benachrichtige bitte daraufhin unseren DevOps, damit er sich deinen Commit anschauen und freigeben kann. Wenn er den Pull Request akzeptiert und merged wird dieser auf das [Live Demo](http://hostpoint.openstream.ch/woocommerce/bitbucketdemo/) deployed, was in einem realen Projekt der Live Shop des Kunden wäre. 

Damit wäre ein kompletter Zyklus des Development Workflows durch.

### Bestehende Dateien ändern ###
Als nächstes führe bitte eine kleine Änderung in deinem Child Theme durch, z.B. eine abweichende Schriftart oder Schriftgrösse in einer CSS-Klasse. Anschliessend für diese Änderung wieder einen Pull Request erstellen und erneut in Rücksprache mit dem DevOps den 2. Zyklus durchführen.

### MySQL Dump erstellen ###
Wenn du z.B. in deiner lokalen Testumgebung auf das neu erstelle Child Theme umstellst, ist diese Änderung in der Live Umgebung noch nicht drin. Bevor ein neuer Shop live geht, macht es manchmal Sinn, dass der Entwickler einen Dump des lokalen Stands der Datenbank erstellt und diesen als dump.sql im versteckten Ordner .db speichert. Dieser Dump kann entweder lokal mit phpMyAdmin oder mit dem mysqldump-Utility direkt auf der Konsole erstellt werden. Dabei wird die bestehenden dump.sql überschrieben und dann wir ebenfalls hierfür ein Commit und Pull Request erstellt.

Wenn während des Live Betriebs eines WooCommerce Shops lokale Konfigurationen geändert werden, führen wir diese in der Regel manuell im Live System nach, nachdem die Code Änderungen via Git deployed wurden. Hier kann es sinnvoll sein, dass der Entwickler in seinem Pull Request Kommentar oder in der Git Commit Message einen Hinweis auf die geänderten Einstellungen gibt. Wenn dieser Kommentar länger ist, ist er aber vermutlich besser in einem JIRA Kommentar aufgehoben.